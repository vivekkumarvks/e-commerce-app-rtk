import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "../features/cart/CartSlice";
import authReducer from "../features/register/RegisterSlice";
import productReducer from "../features/product/ProductSlice";
const store = configureStore({
  reducer: {
    cart: cartReducer,
    auth: authReducer,
    product: productReducer,
  },
});

export default store;
