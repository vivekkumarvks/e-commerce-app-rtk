import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import cartImmer from "immer";

const initialState = {
  loading: false,
  cart: [],
  error: "",
};

export const fetchCarts = createAsyncThunk("cart/fetchCarts", () => {
  axios.defaults.headers = {
    "x-access-token": localStorage.getItem("token"),
  };
  return axios.get("/carts/users").then((response) => response.data.cart);
});

export const placeCartOrders = createAsyncThunk(
  "cart/placeCartOrders",
  (body) => {
    return axios.post("/carts", body).then((response) => {
      return response.data;
    });
  }
);

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      let productIndex = -1;
      let cartProducts =
        JSON.parse(JSON.stringify(state?.cart?.products)) || [];
      cartProducts.forEach((product, index) => {
        if (product.productId === action.payload.productId) {
          productIndex = index;
        }
      });

      if (productIndex !== -1) {
        cartProducts[productIndex].quantity =
          +cartProducts[productIndex].quantity + 1;
      } else {
        let product = {
          productId: action.payload.productId,
          productDetails: action.payload,
          quantity: 1,
        };
        cartProducts.push(product);
      }

      state.cart.products = cartProducts;
    },
    removeFromCart: (state, action) => {
      let cartProducts = JSON.parse(JSON.stringify(state.cart.products));
      cartProducts.splice(action.payload, 1);
      state.cart.products = cartProducts;
    },
    removeAll: (state) => {
      state.cart = [];
    },
    increaseProduct: (state, action) => {
      let cartProducts = JSON.parse(JSON.stringify(state.cart.products));
      cartProducts[action.payload].quantity =
        +cartProducts[action.payload].quantity + 1;

      state.cart.products = cartProducts;
    },
    decreaseProduct: (state, action) => {
      let cartProducts = JSON.parse(JSON.stringify(state.cart.products));
      cartProducts[action.payload].quantity =
        +cartProducts[action.payload].quantity - 1;

      state.cart.products = cartProducts;
    },
    addToCartWithoutLogin: (state, action) => {
      let products = state?.cart?.products || [];

      let product = {
        productId: action.payload.productId,
        productDetails: action.payload,
        quantity: 1,
      };

      products.push(product);

      const newState = cartImmer(state, (copyState) => {
        let newDraft = JSON.parse(JSON.stringify(copyState));
        newDraft.cart.products = products;
        return newDraft;
      });
      return newState;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCarts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchCarts.fulfilled, (state, action) => {
      state.loading = false;
      state.cart = action?.payload ? action.payload : [];
      console.log("Cart Fetched.");
      state.error = "";
    });
    builder.addCase(fetchCarts.rejected, (state, action) => {
      state.loading = false;
      state.cart = [];
      state.error = action.error.message;
    });
    builder.addCase(placeCartOrders.fulfilled, (state, action) => {
      console.log(action.payload);
    });
  },
});

export default cartSlice.reducer;
export const {
  addToCart,
  removeFromCart,
  removeAll,
  increaseProduct,
  decreaseProduct,
  addToCartWithoutLogin,
} = cartSlice.actions;
