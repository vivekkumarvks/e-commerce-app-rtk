import React from "react";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  fetchCarts,
  removeFromCart,
  removeAll,
  increaseProduct,
  decreaseProduct,
  placeCartOrders,
} from "./CartSlice";

const CartView = () => {
  const Cart = useSelector((state) => state);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  let cartProducts = Cart?.cart?.cart?.products || [];
  let discount = 40;
  let deliveryCharge = 50;

  useEffect(() => {
    if (!cartProducts?.length) dispatch(fetchCarts());
  }, []);

  const calculatePrice = () => {
    let price = 0;
    cartProducts?.forEach((item) => {
      price = +price + +item.productDetails.price;
    });

    return price;
  };
  const calculateTotalPrice = () => {
    let price = calculatePrice();
    return price - discount + deliveryCharge;
  };

  const loginBeforePlaceCart = (carttProduct) => {
    let token = localStorage.getItem("token");

    if (token) {
      dispatch(placeCartOrders(carttProduct));
    } else {
      navigate("/login");
    }
  };

  let cartDetails = cartProducts?.map((item, index) => {
    let { productDetails, quantity } = item;

    return (
      <div className="cart-item" key={index}>
        <div className="cart-image-container">
          <img className="cart-item-image" src={productDetails?.productImage} />
          <div className="cart-quantity">
            <button
              className="btn"
              onClick={() => {
                dispatch(decreaseProduct(index));
              }}
            >
              -
            </button>
            <span className="cart-item-qty">{quantity}</span>
            <button
              className="btn"
              onClick={() => {
                dispatch(increaseProduct(index));
              }}
            >
              +
            </button>
          </div>
        </div>
        <div>
          <p className="item-title">{productDetails.title}</p>
          <p className="item-price cart-item-price">
            Price :- ${productDetails.price}
          </p>
          <button
            className="btn btn-accent cart-btn"
            onClick={() => {
              dispatch(removeFromCart(index));
            }}
          >
            Remove From Cart
          </button>
        </div>
      </div>
    );
  });

  let priceDetails = (
    <div className="price-details">
      <span className="bold">Price Details({cartProducts?.length})</span>
      <div className="item-price price-block">
        <div>Price :-</div>
        <div>${calculatePrice()}</div>
      </div>
      <div className="price-block">
        <div>Discount :- </div>
        <div>${discount}</div>
      </div>
      <div className="price-block">
        <div>Delivery Charge :-</div>
        <div> ${deliveryCharge}</div>
      </div>
      <div className="price-block">
        <div className="bold">Total Price :-</div>
        <div>${calculateTotalPrice()}</div>
      </div>
      <div className="price-block clear-cart">
        <button
          className="btn btn-secondary"
          onClick={() => {
            dispatch(removeAll());
          }}
        >
          Clear All Cart
        </button>
        <button
          className="btn"
          onClick={() => {
            loginBeforePlaceCart({ products: cartProducts });
          }}
        >
          Place Your Order
        </button>
      </div>
    </div>
  );

  return (
    <div>
      <div className="cart">
        <div className="cart-details">My Cart {cartDetails}</div>
        {priceDetails}
      </div>
    </div>
  );
};

export default CartView;
