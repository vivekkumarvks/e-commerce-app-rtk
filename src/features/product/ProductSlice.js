import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  loading: false,
  product: [],
  error: "",
};

export const fetchProduct = createAsyncThunk("product/fetchProduct", () => {
  return axios.get("/products").then((response) => {
    return response.data;
  });
});

const productSlice = createSlice({
  name: "product",
  initialState,
  extraReducers: {
    [fetchProduct.fulfilled]: (state, action) => {
      state.loading = false;
      state.error = "";
      state.product = action.payload;
    },
  },
});

export default productSlice.reducer;
