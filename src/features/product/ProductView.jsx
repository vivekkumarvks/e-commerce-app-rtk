import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProduct } from "./ProductSlice";
import {
  addToCart,
  fetchCarts,
  addToCartWithoutLogin,
} from "../cart/CartSlice";

const ProductView = () => {
  const Products = useSelector((state) => state);
  const dispatch = useDispatch();
  const { products } = Products?.product?.product;

  useEffect(() => {
    dispatch(fetchCarts());
    dispatch(fetchProduct());
  }, []);

  const addProductToCart = (product) => {
    let token = localStorage.getItem("token");
    if (token) dispatch(addToCart(product));
    else {
      dispatch(addToCartWithoutLogin(product));
    }
  };

  let product = products?.map((product) => {
    return (
      <div className="product card" key={product.productId}>
        <img
          className="product-image"
          src={`http://localhost:5000/${product.productImage}`}
          alt=""
        />
        <p className="item-name">{product.title}</p>
        <p className="item-price">Price:- {product.price}$</p>
        <div className="action-btn">
          <button
            className="btn btn-primary cart-btn"
            onClick={() => addProductToCart(product)}
          >
            Add to cart
          </button>
          <button className="btn btn-secondary cart-btn">Buy Now</button>
        </div>
      </div>
    );
  });

  return (
    <div>
      <div className="productList">{product}</div>
    </div>
  );
};

export default ProductView;
