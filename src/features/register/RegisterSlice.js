import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  loading: false,
  auth: {},
  error: "",
};

export const registerUser = createAsyncThunk("auth/registerUser", (body) => {
  return axios.post("/register", body).then((response) => response.data);
});

export const loginUser = createAsyncThunk("auth/loginUser", (body) => {
  return axios.post("/login", body).then((response) => {
    return response.data;
  });
});

const setToken = (token) => {
  localStorage.setItem("token", token);
  axios.defaults.headers = {
    "x-access-token": token,
  };
};

const registerSlice = createSlice({
  name: "auth",
  initialState,
  extraReducers: {
    [loginUser.fulfilled]: (state, action) => {
      let { token } = action.payload;
      state.loading = false;
      state.error = "";
      state.auth = token;
      setToken(token);
    },
  },
});

export default registerSlice.reducer;
