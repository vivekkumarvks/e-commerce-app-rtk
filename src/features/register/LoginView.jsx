import React from "react";
import { useForm } from "react-hook-form";
import { TextField, OutlinedInput } from "@material-ui/core";
import { Button, Stack } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { loginUser } from "./RegisterSlice";
import "./register.css";
import { useNavigate } from "react-router-dom";

const LoginView = () => {
  const auth = useSelector((state) => state);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    // formState: { errors },
  } = useForm({});

  const onSubmit = (data) => {
    dispatch(loginUser(data)).then(() => {
      navigate("/");
    });
    console.log(data, auth);
  };

  const form = (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack className="form-field">
        <TextField
          className="form-input"
          {...register("email")}
          label="email"
          variant="outlined"
        />
      </Stack>

      <Stack className="form-field">
        <OutlinedInput
          type="password"
          className="form-input"
          {...register("password")}
          variant="outlined"
          label="Password"
        />
      </Stack>

      <Stack className="form-field">
        <Button type="submit" variant="contained">
          {/* disabled={!formState.isValid} */}
          Submit
        </Button>
      </Stack>
    </form>
  );

  return (
    <div className="login">
      <div className="login-block"></div>
      <div className="login-page">
        <h3>Please Login</h3> {form}
      </div>
      <div className="login-block"></div>
    </div>
  );
};

export default LoginView;
