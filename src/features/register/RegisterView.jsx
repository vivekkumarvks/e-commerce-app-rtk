import React from "react";
import { useForm } from "react-hook-form";
import { TextField } from "@material-ui/core";
import { Button, Stack } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { registerUser } from "./RegisterSlice";
import "./register.css";

const RegisterView = () => {
  const auth = useSelector((state) => state);
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    // formState: { errors },
  } = useForm({});

  const onSubmit = (data) => {
    dispatch(registerUser(data));
  };

  const form = (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Stack className="form-field">
        <TextField
          className="form-input"
          {...register("name")}
          label="Name"
          variant="outlined"
        />
      </Stack>

      <Stack className="form-field">
        <TextField
          className="form-input"
          {...register("email")}
          label="email"
          variant="outlined"
        />
      </Stack>

      <Stack className="form-field">
        <TextField
          className="form-input"
          {...register("mobile")}
          variant="outlined"
          label="Mobile"
        />
      </Stack>

      <Stack className="form-field">
        <TextField
          className="form-input"
          {...register("password")}
          variant="outlined"
          label="Password"
        />
      </Stack>

      <Stack className="form-field">
        <Button type="submit" variant="contained">
          {/* disabled={!formState.isValid} */}
          Submit
        </Button>
      </Stack>
    </form>
  );

  return (
    <div className="block">
      <div className="login">
        <div className="login-block"></div>
        <div className="login-page">
          <h3>Please Register</h3> {form}
        </div>
        <div className="login-block"></div>
      </div>
    </div>
  );
};

export default RegisterView;
