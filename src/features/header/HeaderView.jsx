import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const HeaderView = () => {
  const selector = useSelector((state) => state);
  const navigate = useNavigate();
  let cartProducts = selector?.cart?.cart?.products || [];

  console.log("Header product", selector?.cart);
  const goToCart = () => {
    navigate("/cart");
  };
  const checkToken = () => {
    let token = localStorage.getItem("token");
    return token ? "Vivek" : "Login";
  };
  return (
    <div>
      <header className="header">
        <div className="header-logo">
          <h2>Amazon</h2>
        </div>
        <div>
          <button className="btn">{checkToken()}</button>
          <button className="btn" onClick={() => goToCart()}>
            Cart({cartProducts.length})
          </button>
        </div>
      </header>
    </div>
  );
};

export default HeaderView;
