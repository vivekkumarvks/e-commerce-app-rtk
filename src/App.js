import axios from "axios";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import Header from "./features/header/HeaderView";
import Cart from "./features/cart/CartView";
import Product from "./features/product/ProductView";
import Login from "./features/register/LoginView";
import Register from "./features/register/RegisterView";

axios.defaults.baseURL = "http://localhost:5000";

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<Product />} />

        <Route path="/cart" element={<Cart />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
      </Routes>
    </div>
  );
}

export default App;
